[![NoItemDespawn](https://github.com/Jomcraft-Network/NoItemDespawn/actions/workflows/build.yml/badge.svg)](https://github.com/Jomcraft-Network/NoItemDespawn/actions/workflows/build.yml)

### NoItemDespawn

---

NoItemDespawn is a Minecraft Forge mod created by the Jomcraft Network development team. Official downloads and further information may be found on our [curseforge page](https://www.curseforge.com/minecraft/mc-mods/noitemdespawn).

##### License

This project is licensed under the **Apache License Version 2.0** license. We do not grant any type of warranty.

##### Future

Our git repository has been moved to GitHub. Please visit [this page](https://github.com/Jomcraft-Network/NoItemDespawn). The GitLab presence won't be updated further!
